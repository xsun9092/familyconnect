var gCtx = null;
var gCanvas = null;
var canvas_inited=false;
var gUM=false;
var webkit=false;
var moz=false;
var v=null;
var interval=200;
var DecodeWorker;

function initCanvas(w,h)
{
    gCanvas = document.getElementById("qr-canvas");
    gCanvas.style.width = w + "px";
    gCanvas.style.height = h + "px";
    gCanvas.width = w;
    gCanvas.height = h;
    gCtx = gCanvas.getContext("2d");
    gCtx.clearRect(0, 0, w, h);
}

function captureToCanvas() {
    if(!canvas_inited)
        return;
    if(gUM)
    {
        gCtx.drawImage(v,0,0);
        DecodeWorker.postMessage({ImageData: gCtx.getImageData(0,0,gCanvas.width,gCanvas.height).data, Width: gCanvas.width, Height: gCanvas.height, cmd: "normal"});
    }
}

function read(a)
{
    document.getElementById("submit_qr").disabled=false;
    document.getElementById("qr-value").value=a;
}	

function isCanvasSupported(){
  var elem = document.createElement('canvas');
  return !!(elem.getContext && elem.getContext('2d'));
}

function success(stream) {
    if (webkit)
        v.src = window.webkitURL.createObjectURL(stream);
    else if (moz) {
        v.mozSrcObject = stream;
        v.play();
    }
    else
        v.src = stream;
    gUM=true;
    setTimeout(captureToCanvas, interval);
}
		
function error(error) {
    gUM=false;
    return;
}

function receiveMessage(e) {
    alert(JSON.stringify(e));
    setTimeout(captureToCanvas, interval);
}

function load()
{
    document.getElementById("submit_qr").disabled=true;
	if(isCanvasSupported() && window.File && window.FileReader)
	{
		initCanvas(800, 600);
		document.getElementById("mainbody").style.display="inline";
        DecodeWorker = new Worker("/js/lib/BarcodeReader/DecoderWorker.js");
        DecodeWorker.onmessage = receiveMessage;
        setwebcam();
	}
	else
	{
		document.getElementById("mainbody").style.display="inline";
		document.getElementById("mainbody").innerHTML='<h2 class="text-center">QR code scanner for HTML5 capable browsers<br>'+
        '<small>sorry your browser is not supported</small></h2>'+
        '<h3 class="text-center">Try <br>'+
        '<a href="http://www.mozilla.com/firefox"><img src="https://mozorg.cdn.mozilla.net/media/img/firefox/new/header-firefox.png"/></a> <br>or<br> '+
        '<a href="http://chrome.google.com"><img src="https://www.google.com/chrome/assets/common/images/chrome_logo_2x.png"/></a> <br>or<br> '+
        '<a href="http://www.opera.com"><img src="http://www-static.opera.com/static-heap/84/842f753da22ec401f54edaf992959b1ec24d3f94/opera-software-logo.png"/></a></h3>';
	}
}

function setwebcam()
{
	document.getElementById("qr-value").value="";
    if(canvas_inited)
    {
        setTimeout(captureToCanvas, interval);    
        return;
    }
    var n=navigator;
    v=document.getElementById("camsource");

    if(n.getUserMedia)
        n.getUserMedia({video: true, audio: false}, success, error);
    else
    if(n.webkitGetUserMedia)
    {
        webkit=true;
        n.webkitGetUserMedia({video: true, audio: false}, success, error);
    }
    else
    if(n.mozGetUserMedia)
    {
        moz=true;
        n.mozGetUserMedia({video: true, audio: false}, success, error);
    }

    canvas_inited=true;
    setTimeout(captureToCanvas, interval);
}
