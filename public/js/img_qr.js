var cavas_inited=false;

function dragenter(e) {
  e.stopPropagation();
  e.preventDefault();
}

function dragover(e) {
  e.stopPropagation();
  e.preventDefault();
}

function drop(e) {
  e.stopPropagation();
  e.preventDefault();

  var dt = e.dataTransfer;
  var files = dt.files;
  if(files.length>0)
  {
	handleFiles(files);
  }
  else if(dt.getData('URL'))
  {
	qrcode.decode(dt.getData('URL'));
  }
}

function handleFiles(f)
{
	for(var i=0;i<f.length;i++)
	{
        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
    			qrcode.decode(e.target.result);
            };
        })(f[i]);
        reader.readAsDataURL(f[i]);	
    }
}

function read(a)
{
    document.getElementById("submit_qr").disabled=false;
    document.getElementById("qr-value").value=a;
}

function isCanvasSupported(){
  var elem = document.createElement('canvas');
  return !!(elem.getContext && elem.getContext('2d'));
}

function load()
{
    document.getElementById("submit_qr").disabled=true;
    if(isCanvasSupported() && window.File && window.FileReader)
    {
        qrcode.callback = read;
        document.getElementById("mainbody").style.display="inline";
        setimg();
    }
    else
    {
        document.getElementById("mainbody").style.display="inline";
        document.getElementById("mainbody").innerHTML='<h2 class="text-center">QR code scanner for HTML5 capable browsers<br>'+
        '<small>sorry your browser is not supported</small></h2>'+
        '<h3 class="text-center">Try <br>'+
        '<a href="http://www.mozilla.com/firefox"><img src="https://mozorg.cdn.mozilla.net/media/img/firefox/new/header-firefox.png"/></a> <br>or<br> '+
        '<a href="http://chrome.google.com"><img src="https://www.google.com/chrome/assets/common/images/chrome_logo_2x.png"/></a> <br>or<br> '+
        '<a href="http://www.opera.com"><img src="http://www-static.opera.com/static-heap/84/842f753da22ec401f54edaf992959b1ec24d3f94/opera-software-logo.png"/></a></h3>';
    }
}

function setimg()
{
    if(cavas_inited)
        return;
    var qrfile = document.getElementById("qrfile");
    qrfile.addEventListener("dragenter", dragenter, false);  
    qrfile.addEventListener("dragover", dragover, false);  
    qrfile.addEventListener("drop", drop, false);
    cavas_inited=true;
}
