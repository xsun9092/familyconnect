Parse.initialize("lownJ7tCftTpPppL7rm2NfMIxUuyyTbuxkKNQOcJ", "qBPUEVIZwD0L6MYBoef0ia7Rn7Pb1XRnfS53NTid");

function sendFileToParse(file, success, failure) {
  if (file.length > 0) {
    var parseFile = new Parse.File(file[0].name, file[0]);
    parseFile.save().then(function() {
      // The file has been saved to Parse.
      var fileInstance = new Parse.Object("AppFiles");
      fileInstance.set("file", parseFile);
      fileInstance.save().then(function(obj) {
        if (success) {
          success(obj);
        }
      }, function(error) {
        if (failure) {
          failure(error);
        }
      });
    }, function(error) {
      if (failure) {
        failure(error);
      }
    });
  }
}

