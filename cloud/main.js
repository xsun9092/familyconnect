require('cloud/app.js');
var Constant = require('cloud/util/constant');
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

Parse.Cloud.afterSave("QAState", function(request) {
	if ((request.object.get("elder_state") == Constant.QAState.Correct) && (request.object.get("younger_state") == Constant.QAState.Correct)) {
		var myTeamGot = false;
		var myTeam = request.object.get("team");
		var thisQuestionGot = false;
		var thisQuestion = request.object.get("question");
		myTeam.fetch({
			success: function(myTeam) {
				myTeamGot = true;
				addMark(myTeamGot, thisQuestionGot, myTeam, thisQuestion);
			},
			error: function(myTeam, error) {
				console.error("Error when getting team info: " + error);
			}
		});
		thisQuestion.fetch({
			success: function(thisQuestion) {
				thisQuestionGot = true;
				addMark(myTeamGot, thisQuestionGot, myTeam, thisQuestion);
			},
			error: function(thisQuestion, error) {
				console.error("Error when getting question info: " + error);
			}
		});
	}
});

function addMark(myTeamGot, thisQuestionGot, myTeam, thisQuestion) {
	if (myTeamGot && thisQuestionGot) {
		var originalMark = myTeam.get("mark");
		myTeam.set("mark", originalMark + thisQuestion.get("mark"));
		myTeam.save();
	}
}

