var Constant = require('cloud/util/constant');

exports.Choice = function (choicesArray, markingType, answer) {
	this.qtype = Constant.QustionType.Choice;
	this.choices = choicesArray;
	this.markingType = markingType;
	this.answer = answer;
};

exports.FillBlank = function (markingType, answer) {
	this.qtype = Constant.QustionType.FillInBlank;
	this.markingType = markingType;
	this.answer = answer;
};

exports.File = function () {
	this.qtype = Constant.QustionType.File;

};

exports.QR = function (markingType, answer) {
	this.qtype = Constant.QustionType.QR;
	this.markingType = markingType;
	this.answer = answer;
};