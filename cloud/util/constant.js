exports.QAState = Object.freeze({
	NotAnswered		: 0,
	Correct			: 1,
	Wrong			: 2,
	WaitingMarking	: 3
});

exports.UserType = Object.freeze({
	Invalid			:-1,
	NormalUser		: 0,
	Admin			: 1
});

exports.QustionType = Object.freeze({
	Invalid			:-1,
	Choice			: 0,
	FillInBlank		: 1,
	File			: 2,
	QR				: 3
});

exports.MarkingType = Object.freeze({
	Invalid			:-1,
	Auto			: 0,
	Manual			: 1
});
