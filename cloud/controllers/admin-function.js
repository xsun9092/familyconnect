var Constant = require('cloud/util/constant');
var Question = require('cloud/util/question');

exports.getDashboardView = function(req, res) {
	var currentAdmin = Parse.User.current();
	res.render('admin-dashboard.ejs', {firstname: currentAdmin.get("firstName"), lastname: currentAdmin.get("lastName")});
};

exports.getTeamListView = function(req, res) {
	var query_AllTeams = new Parse.Query("Team");
	query_AllTeams.include("elder");
	query_AllTeams.include("younger");
	query_AllTeams.find({
		success: function (queryResult_AllTeams) {
			var teamsInfo = new Array();
			for (var i = 0; i < queryResult_AllTeams.length; i++) {
				var elder = queryResult_AllTeams[i].get("elder");
				var younger = queryResult_AllTeams[i].get("younger");
				teamsInfo[i] = {name: queryResult_AllTeams[i].get("name"), elder_id: elder.id, elder_firstname: elder.get("firstName"), elder_lastname: elder.get("lastName"), 
								younger_id: younger.id, younger_firstname: younger.get("firstName"), younger_lastname: younger.get("lastName"), mark: queryResult_AllTeams[i].get("mark")};
			};
			res.render('admin-view-all-teams.ejs', {teams: teamsInfo});
		},
		error: function (allTeamInfoError) {
			console.error("Error when getting all team info: " + allTeamInfoError);
		}
	});
};

function tryShowAllUsersInfo(allTeamsInfo, allUsersInfo, isTeamInfoFetched, isUserInfoFeteched, res) {
	if (isTeamInfoFetched && isUserInfoFeteched) {
		var usersInfo = new Array();
		for (var i = 0; i < allUsersInfo.length; i++) {
			var teams = new Array();
			var thisUserId = allUsersInfo[i].id;
			for (var j = 0; j < allTeamsInfo.length; j++) {
				if ((thisUserId == allTeamsInfo[j].elder_id) || (thisUserId == allTeamsInfo[j].younger_id)) {
					teams.push({id: allTeamsInfo[j].id, name: allTeamsInfo[j].name});
				}
			};
			usersInfo[i] = {email: allUsersInfo[i].get("email"), firstname: allUsersInfo[i].get("firstName"), lastname: allUsersInfo[i].get("lastName"), teams: teams};
		};
		res.render('admin-view-all-users.ejs', {users: usersInfo});
	}
}

exports.getUserListView = function(req, res) {
	// team info
	var allTeamsInfo;
	var allUsersInfo;
	var isTeamInfoFetched = false;
	var isUserInfoFeteched = false;
	var query_AllTeams = new Parse.Query("Team");
	query_AllTeams.include("elder");
	query_AllTeams.include("younger");
	query_AllTeams.find({
		success: function (queryResult_AllTeams) {
			allTeamsInfo = new Array();
			for (var i = 0; i < queryResult_AllTeams.length; i++) {
				allTeamsInfo[i] = {id: queryResult_AllTeams[i].id, name: queryResult_AllTeams[i].get("name"), elder_id: queryResult_AllTeams[i].get("elder").id, younger_id: queryResult_AllTeams[i].get("younger").id};
			};
			isTeamInfoFetched = true;
			tryShowAllUsersInfo(allTeamsInfo, allUsersInfo, isTeamInfoFetched, isUserInfoFeteched, res);
		},
		error: function (allTeamInfoError) {
			console.error("Error when getting all team info: " + allTeamInfoError);
		}
	});

	// user info
	var query_AllUsers = new Parse.Query("User");
	query_AllUsers.equalTo("type", 0);
	query_AllUsers.find({
		success: function (queryResult_AllUsers) {
			allUsersInfo = queryResult_AllUsers;
			isUserInfoFeteched = true;
			tryShowAllUsersInfo(allTeamsInfo, allUsersInfo, isTeamInfoFetched, isUserInfoFeteched, res);
		},
		error: function (allUserInfoError) {
			console.error("Error when getting all user info: " + allUserInfoError);
		}
	});
};

function tryShowThisUsersInfo(teamInfo, userInfo, isTeamInfoFetched, isUserInfoFeteched, res) {
	if (isTeamInfoFetched && isUserInfoFeteched) {
		res.render('admin-view-user.ejs', {firstname: userInfo.firstname, lastname: userInfo.lastname, email: userInfo.email, teams: teamInfo, badges: userInfo.badges});
	}
}

exports.getSingleUserView = function(req, res) {
	// user info
	var userInfo;
	var teamInfo;
	var isUserInfoFeteched = false;
	var isTeamInfoFetched = false;
	var query_ThisUser = new Parse.Query("User");
	query_ThisUser.get (req.params.user_id, {
		success: function (obj_thisUser) {
			userInfo = {firstname: obj_thisUser.get("firstName"), lastname: obj_thisUser.get("lastName"), email: obj_thisUser.get("email"), badges: new Array()};
			if (obj_thisUser.get("badgeCount") > 0) {
				var relation_badges = obj_thisUser.relation("badges");
				relation_badges.query().find({
					success: function (queryResult_BadgeInfo) {
						for (var bli = 0; bli < queryResult_BadgeInfo.length; bli++) {
							userInfo.badges.push(queryResult_BadgeInfo[bli].get("name"));
						}
						isUserInfoFeteched = true;
						tryShowThisUsersInfo(teamInfo, userInfo, isTeamInfoFetched, isUserInfoFeteched, res);
					},
					error: function (badgeInfoError) {
						console.error("Error when getting badge info: " + badgeInfoError);
					}
				});
			} else {
				isUserInfoFeteched = true;
				tryShowThisUsersInfo(teamInfo, userInfo, isTeamInfoFetched, isUserInfoFeteched, res);
			}
		},
		error: function (user_error) {
			console.error("Error when getting user info: " + user_error);
		}
	});

	// team info
	var thisUser = new Parse.User();
	thisUser.id = req.params.user_id;
	var query_IsElder = new Parse.Query("Team");
	query_IsElder.equalTo("elder", thisUser);
	var query_IsYounger = new Parse.Query("Team");
	query_IsElder.equalTo("younger", thisUser);
	var query_UserTeams = Parse.Query.or(query_IsElder, query_IsYounger);
	query_UserTeams.find ({
		success: function (queryResult_Teams) {
			teamInfo = new Array();
			for (var i = 0; i < queryResult_Teams.length; i++) {
				teamInfo[i] = {id: queryResult_Teams[i].id, name: queryResult_Teams[i].get("name")};
			};
			isTeamInfoFetched = true;
			tryShowThisUsersInfo(teamInfo, userInfo, isTeamInfoFetched, isUserInfoFeteched, res);
		},
		error: function (teamInfoError) {
			console.error("Error when getting team info: " + teamInfoError);
		}
	});
};

exports.getSingleTeamView = function(req, res) {
	var query_ThisTeam = new Parse.Query("Team");
	query_ThisTeam.include("elder");
	query_ThisTeam.include("younger");
	query_ThisTeam.get (req.params.team_id, {
		success: function (obj_thisTeam) {
			var elder = obj_thisTeam.get("elder");
			var younger = obj_thisTeam.get("younger");
			res.render('admin-view-team.ejs', {name: obj_thisTeam.get("name"), elder_id: elder.id, elder_firstname: elder.get("firstName"), elder_lastname: elder.get("lastName"), 
											   younger_id: younger.id, younger_firstname: younger.get("firstName"), younger_lastname: younger.get("lastName"), mark: obj_thisTeam.get("mark")});
		},
		error: function (team_error) {
			console.error("Error when getting team info: " + team_error);
		}
	});
};

exports.getNewQuestionView = function(req, res) {
	res.render('admin-new-question.ejs', {msg: req.query.msg});
};

function setNewQuestionForAllTeams(res, allTeamsInfo, newQuestionInfo, isAllTeamInfoGot, isQuestionSaved) {
	if (isAllTeamInfoGot && isQuestionSaved) {
		if (allTeamsInfo.length > 0) {
			var QAState = Parse.Object.extend("QAState");
			var newQAStates = new Array();
			for (var i = 0; i < allTeamsInfo.length; i++) {
				var newQA = new QAState();
				newQA.set("elder_state", Constant.QAState.NotAnswered);
				newQA.set("younger_state", Constant.QAState.NotAnswered);
				newQA.set("team", allTeamsInfo[i]);
				newQA.set("question", newQuestionInfo);
				newQAStates.push(newQA);
			}
			Parse.Object.saveAll(newQAStates, {
				success: function(list) {
					res.redirect('/admin/dashboard');
				},
				error: function(error) {
					console.error("Error when saving all QAStates: " + error);
				},
			});
		} else {
			res.redirect('/admin/dashboard');
		}
	}
}

function ParseQuestionType(type) {
	switch(type) {
		case "choice":
			return Constant.QustionType.Choice;
		case "fill":
			return Constant.QustionType.FillInBlank;
		case "file":
			return Constant.QustionType.File
		case "qr":
			return Constant.QustionType.QR;
		default:
			return Constant.QustionType.Invalid;
	}
}

function ParseMarkingType_Elder(req) {
	switch(req.body.checking_elder) {
		case "manual":
			return Constant.MarkingType.Manual;
		case "auto": {
				switch (req.body.type_elder) {
					case "choice":
						var answer = parseInt(req.body.answer_elder_choice);
						if (answer >= 0) {
							var i = 0;
							while (true) {
								if (!req.body.hasOwnProperty("choice" + i + "_elder")) {
									break;
								}
								i++;
							}
							if (answer < i) {
								return Constant.MarkingType.Auto;
							}
						}
						return Constant.MarkingType.Invalid;
					case "fill":
						if (req.body.answer_elder_fill.length > 0) {
							return Constant.MarkingType.Auto;
						}
						return Constant.MarkingType.Invalid;
					case "file":
						return Constant.MarkingType.Auto;
					case "qr":
						if (req.body.answer_elder_qr.length > 0) {
							return Constant.MarkingType.Auto;
						}
						return Constant.MarkingType.Invalid;
					default:
						return Constant.MarkingType.Invalid;
				}
			}
			return Constant.MarkingType.Invalid;
		default:
			return Constant.MarkingType.Invalid;
	}
}

function ParseMarkingType_Younger(req) {
	switch(req.body.checking_younger) {
		case "manual":
			return Constant.MarkingType.Manual;
		case "auto": {
				switch (req.body.type_younger) {
					case "choice":
						var answer = parseInt(req.body.answer_younger_choice);
						if (answer >= 0) {
							var i = 0;
							while (true) {
								if (!req.body.hasOwnProperty("choice" + i + "_younger")) {
									break;
								}
								i++;
							}
							if (answer < i) {
								return Constant.MarkingType.Auto;
							}
						}
						return Constant.MarkingType.Invalid;
					case "fill":
						if (req.body.answer_younger_fill.length > 0) {
							return Constant.MarkingType.Auto;
						}
						return Constant.MarkingType.Invalid;
					case "file":
						return Constant.MarkingType.Auto;
					case "qr":
						if (req.body.answer_younger_qr.length > 0) {
							return Constant.MarkingType.Auto;
						}
						return Constant.MarkingType.Invalid;
					default:
						return Constant.MarkingType.Invalid;
				}
			}
			return Constant.MarkingType.Invalid;
		default:
			return Constant.MarkingType.Invalid;
	}
}

function MakeQuestionInfo_Elder(req, markingType) {
	switch(req.body.type_elder) {
		case "choice":
			var i = 0;
			var choices = new Array();
			while (true) {
				var propertyName = "choice" + i + "_elder";
				if (req.body.hasOwnProperty(propertyName)) {
					choices.push(req.body[propertyName]);
				} else {
					break;
				}
				i++;
			}
			return new Question.Choice(choices, markingType, parseInt(req.body.answer_elder_choice));
		case "fill":
			return new Question.FillBlank(markingType, req.body.answer_elder_fill);
		case "file":
			return new Question.File();
		case "qr":
			return new Question.QR(markingType, req.body.answer_elder_qr);
		default:
			return Undefinded;
	}
}

function MakeQuestionInfo_Younger(req, markingType) {
	switch(req.body.type_younger) {
		case "choice":
			var i = 0;
			var choices = new Array();
			while (true) {
				var propertyName = "choice" + i + "_younger";
				if (req.body.hasOwnProperty(propertyName)) {
					choices.push(req.body[propertyName]);
				} else {
					break;
				}
				i++;
			}
			return new Question.Choice(choices, markingType, parseInt(req.body.answer_younger_choice));
		case "fill":
			return new Question.FillBlank(markingType, req.body.answer_younger_fill);
		case "file":
			return new Question.File();
		case "qr":
			return new Question.QR(markingType, req.body.answer_younger_qr);
		default:
			return Undefinded;
	}
}

exports.createNewQuestion = function(req, res) {

	if ((req.body.title_elder.length > 0) && (req.body.title_younger.length > 0) && (req.body.mark.length > 0)) {

		var mark = parseInt(req.body.mark);

		if ((NaN != mark) && (mark >= 0)) {

			var question_type_elder = ParseQuestionType(req.body.type_elder);
			var question_type_younger = ParseQuestionType(req.body.type_younger);

			if ((Constant.QustionType.Invalid != question_type_elder) && (Constant.QustionType.Invalid != question_type_younger)) {
				var marking_type_elder = ParseMarkingType_Elder(req);
				var marking_type_younger = ParseMarkingType_Younger(req);
				if ((Constant.MarkingType.Invalid != marking_type_elder) && (Constant.MarkingType.Invalid != marking_type_younger)) {
					var allTeamsInfo;
					var newQuestionInfo;
					var isAllTeamInfoGot = false;
					var isQuestionSaved = false;
					// get team list
					var query_AllTeams = new Parse.Query("Team");
					query_AllTeams.find ({
						success: function (queryResult_AllTeams) {
							allTeamsInfo = queryResult_AllTeams;
							isAllTeamInfoGot = true;
							setNewQuestionForAllTeams(res, allTeamsInfo, newQuestionInfo, isAllTeamInfoGot, isQuestionSaved);
						},
						error: function (teamInfoError) {
							console.error("Error when getting all teams info: " + teamInfoError);
						}
					});

					// create new question
					var Question = Parse.Object.extend("Question");
					var newQuestion = new Question();
					var questionInfoOK = false;

					var shouldSaveImg = false;
					var image;
					if (req.body.img_id) {
						shouldSaveImg = true;
						var fileInstance = new Parse.Object("AppFiles");
						fileInstance.id = req.body.img_id;
						fileInstance.fetch({
							success: function(fileInstance) {
								image = fileInstance.get("file");
								saveNewQuestion(shouldSaveImg, image, questionInfoOK, newQuestion, res, allTeamsInfo, newQuestionInfo, isAllTeamInfoGot, isQuestionSaved);
							},
							error: function(fileInstance, error) {
								console.error("Error when getting image file: " + error);
							}
						});
					}
					var q_info_elder = MakeQuestionInfo_Elder(req);
					var q_info_younger = MakeQuestionInfo_Younger(req);
					
					newQuestion.set("title_elder", req.body.title_elder);
					newQuestion.set("title_younger", req.body.title_younger);
					newQuestion.set("description_elder", req.body.description_elder);
					console.error("description_younger: " + req.body.description_younger);
					newQuestion.set("description_younger", req.body.description_younger);
					newQuestion.set("question_type_elder", question_type_elder);
					newQuestion.set("question_type_younger", question_type_younger);
					newQuestion.set("marking_type_elder", marking_type_elder);
					newQuestion.set("marking_type_younger", marking_type_younger);
					newQuestion.set("question_info_elder", JSON.stringify(MakeQuestionInfo_Elder(req, marking_type_elder)));
					newQuestion.set("question_info_younger", JSON.stringify(MakeQuestionInfo_Younger(req, marking_type_younger)));
					newQuestion.set("mark", mark);
					
					if (req.body.tags) {
						var tags = req.body.tags.split(",");
						newQuestion.set("tags", tags);
					}
					questionInfoOK = true;
					saveNewQuestion(shouldSaveImg, image, questionInfoOK, newQuestion, res, allTeamsInfo, newQuestionInfo, isAllTeamInfoGot, isQuestionSaved);
				} else {
					res.redirect('/admin/new-question?msg=4');
				}
			} else {
				res.redirect('/admin/new-question?msg=2');
			}
		} else {
			res.redirect('/admin/new-question?msg=1');
		}
	} else {
		res.redirect('/admin/new-question?msg=3');
	}
};


function saveNewQuestion(shouldSaveImg, image, questionInfoOK, newQuestion, res, allTeamsInfo, newQuestionInfo, isAllTeamInfoGot, isQuestionSaved) {
	if (shouldSaveImg) {
		if (image && questionInfoOK) {
			newQuestion.set("image", image);
		} else {
			return;
		}
	}
	newQuestion.save(null, {
		success: function(newQuestion) {
			newQuestionInfo = newQuestion;
			isQuestionSaved = true;
			setNewQuestionForAllTeams(res, allTeamsInfo, newQuestionInfo, isAllTeamInfoGot, isQuestionSaved);
		},
		error: function(newQuestion, error) {
			console.error("Error when saving new question object: " + error);
		}
	});
}

exports.getNeedMarking = function(req, res) {
	var canState = [Constant.QAState.WaitingMarking, Constant.QAState.Correct];


	var query_elderNeed = new Parse.Query("QAState");
	query_elderNeed.equalTo("elder_state", Constant.QAState.WaitingMarking);
	var query_youngerCan = new Parse.Query("QAState");
	query_youngerCan.containedIn("younger_state", canState);
	query_youngerCan.matchesKeyInQuery("elder_state", "elder_state", query_elderNeed);


	var query_youngerNeed = new Parse.Query("QAState");
	query_youngerNeed.equalTo("younger_state", Constant.QAState.WaitingMarking);
	var query_elderCan = new Parse.Query("QAState");
	query_elderCan.containedIn("elder_state", canState);
	query_elderCan.matchesKeyInQuery("younger_state", "elder_state", query_youngerNeed);


	var query_QAState = Parse.Query.or(query_youngerCan, query_elderCan);
	query_QAState.include("team");
	query_QAState.include("question");
	query_QAState.find({
		success: function (queryResult_QAState) {
			var qaStateInfo = new Array();
			for (var i = 0; i < queryResult_QAState.length; i++) {
				var question = queryResult_QAState[i].get("question");
				var questionName_elder = question.get("title_elder");
				var questionName_younger = question.get("title_younger");
				var teamName = queryResult_QAState[i].get("team").get("name");
				qaStateInfo[i] = {questionName_elder: questionName_elder, questionName_younger: questionName_younger, teamName: teamName, id: queryResult_QAState[i].id};
			};
			res.render('admin-view-need-marking.ejs', {states: qaStateInfo});
		},
		error: function (allTeamInfoError) {
			console.error("Error when getting QAstate info: " + allTeamInfoError);
		}
	});
}

function getAnswser(questionInfo, answer, success, failure) {
	var question = JSON.parse(questionInfo);
	switch (question.qtype) {
		case Constant.QustionType.Choice:
			success(question.choices[parseInt(answer)]);
			break;
		case Constant.QustionType.FillInBlank:
			success(answer);
			break;
		case Constant.QustionType.QR:
			success(answer);
			break;
		case Constant.QustionType.File:
			var fileInstance = new Parse.Object("AppFiles");
			fileInstance.id = answer;
			fileInstance.fetch({
				success: function(fileInstance) {
					success("<a href=\"" + fileInstance.get("file").url() + "\">Download</a>");
				},
				error: function(fileInstance, error) {
					console.error("Error when getting file: " + error);
					if (failure) {
						failure("can not get file, error: " + error);
					}
				}
			});
			break;
		default:
			if (failure) {
				failure("Invalid question type");
			}
	}
}

exports.getMarkingQuestionView = function(req, res) {
	var query_ThisQAState = new Parse.Query("QAState");
	query_ThisQAState.include("question");
	query_ThisQAState.include("team");
	query_ThisQAState.get (req.params.qaState_id, {
		success: function (obj_thisQAState) {
			var question = obj_thisQAState.get("question");
			var team = obj_thisQAState.get("team");
			var isElderGot = false;
			var isYoungerGot = false;
			var answer_elder;
			var answer_younger;
			getAnswser(question.get("question_info_elder"), obj_thisQAState.get("elder_answer"), function(answer) {
				answer_elder = answer;
				isElderGot = true;
				renderMarkingQuestionView(isElderGot, isYoungerGot, answer_elder, answer_younger, question, res, req.params.qaState_id);
			});
			
			getAnswser(question.get("question_info_younger"), obj_thisQAState.get("younger_answer"), function(answer) {
				answer_younger = answer;
				isYoungerGot = true;
				renderMarkingQuestionView(isElderGot, isYoungerGot, answer_elder, answer_younger, question, res, req.params.qaState_id);
			});
			
		},
		error: function (team_error) {
			console.error("Error when getting QAState info: " + team_error);
		}
	});
}

function renderMarkingQuestionView(isElderGot, isYoungerGot, answer_elder, answer_younger, question, res, id) {
	if (isElderGot && isYoungerGot) {
		res.render('admin-mark-question.ejs', {title_elder: question.get("title_elder"), title_younger: question.get("title_younger"), description_elder: question.get("description_elder"),
				description_younger: question.get("description_younger"), answer_elder: answer_elder, answer_younger: answer_younger, id: id});
	}
}

exports.markQuestion = function(req, res) {
	if (req.body.correct) {
		var correct = parseInt(req.body.correct);
		if (1 == correct || 0 == correct) {
			var query_ThisQAState = new Parse.Query("QAState");
			query_ThisQAState.include("question");
			query_ThisQAState.include("team");
			query_ThisQAState.get (req.params.qaState_id, {
				success: function (obj_thisQAState) {
					if (obj_thisQAState.get("elder_state") == Constant.QAState.WaitingMarking) {
						obj_thisQAState.set("elder_state", (1 == correct) ? Constant.QAState.Correct : Constant.QAState.Wrong);
					}
					if (obj_thisQAState.get("younger_state") == Constant.QAState.WaitingMarking) {
						obj_thisQAState.set("younger_state", (1 == correct) ? Constant.QAState.Correct : Constant.QAState.Wrong);
					}
					obj_thisQAState.save(null, {
						success: function(obj_thisQAState) {
							res.redirect('/admin/need-marking');
						},
						error: function(obj_thisQAState, error) {
							console.error("Error when saving new QAState object: " + error);
						}
					});
				},
				error: function (team_error) {
					console.error("Error when getting QAState info: " + team_error);
				}
			});
		} else {
			console.error("error in markQuestion, correct=" + correct);
		}
	}
}
