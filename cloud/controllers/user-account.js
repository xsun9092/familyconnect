var Constant = require('cloud/util/constant');

exports.getRegisterView = function(req, res) {
	// Renders the login form asking for username and password.
	res.render('register.ejs', {msg: req.query.msg});
};

exports.getLoginView = function(req, res) {
	// Renders the login form asking for username and password.
	res.render('login.ejs', {msg: req.query.msg});
};

exports.register = function(req, res) {
	if ((req.body.username.length > 0) && (req.body.password.length > 0) && (req.body.passwordconfirm.length > 0) && (req.body.firstname.length > 0) && (req.body.lastname.length > 0)) {
		if (req.body.password == req.body.passwordconfirm) {
			if (Parse.User.current()) {
				Parse.User.logOut();
			}
			var user = new Parse.User();
			user.set("username", req.body.username);
			user.set("password", req.body.password);
			user.set("email", req.body.username);
			user.set("firstName", req.body.firstname);
			user.set("lastName", req.body.lastname);
			user.set("badgeCount", 0);
			user.set("type", Constant.UserType.NormalUser);
			user.signUp(null, {
				success: function(user) {
					res.redirect('/dashboard');
				},
				error: function(user, error) {
					res.redirect('/register?msg=1');
				}
			});
		} else {
			res.redirect('/register?msg=2');
		}
	} else {
		res.redirect('/register?msg=3');
	}
};

exports.login = function(req, res) {
	Parse.User.logIn(req.body.username, req.body.password, {
		success: function(user) {
			if (Constant.UserType.NormalUser == user.get("type")) {
				res.redirect('/dashboard');
			} else {
				res.redirect('/login?msg=1');
			}
		},
		error: function(user, error) {
			res.redirect('/login?msg=1');
		}
	});
};

exports.logout = function(req, res) {
	var currentUser = Parse.User.current();
	var type = Constant.UserType.Invalid;
	if (currentUser) {
		type = currentUser.get("type");
	}
	Parse.User.logOut();
	switch (type) {
	case 0:
		res.redirect('/login?msg=2');
		break;
	case 1:
		res.redirect('/admin-login?msg=2');
		break;
	default:
		res.redirect('/login?msg=2');
		break;
	}
	
};