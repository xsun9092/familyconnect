var Constant = require('cloud/util/constant');
var UAParser = require('cloud/lib/ua-parser');

exports.getCreateTeamView = function(req, res) {
	var currentUser = Parse.User.current();
	if (currentUser && (0 == currentUser.get("type"))) {
		res.render('create_team.ejs', {msg: req.query.msg});
	} else {
		res.redirect('/login?msg=3');
	}
};

function setNewTeamQuestions(res, newTeam, questionList, isTeamSaved, isQuestionListGot) {
	if (isTeamSaved && isQuestionListGot) {
		if (questionList.length > 0) {
			var QAState = Parse.Object.extend("QAState");
			var newQAStates = new Array();
			for (var i = 0; i < questionList.length; i++) {
				var newQA = new QAState();
				newQA.set("elder_state", Constant.QAState.NotAnswered);
				newQA.set("younger_state", Constant.QAState.NotAnswered);
				newQA.set("team", newTeam);
				newQA.set("question", questionList[i]);
				newQAStates.push(newQA);
			}
			Parse.Object.saveAll(newQAStates, {
				success: function(list) {
					res.redirect('/dashboard');
				},
				error: function(error) {
					console.error("Error when saving all QAStates: " + error);
				},
			});
		} else {
			res.redirect('/dashboard');
		}
	}
}

exports.createTeam = function(req, res) {
	var currentUser = Parse.User.current();
	if (currentUser && (0 == currentUser.get("type"))) {
		if ((req.body.teamname.length > 0) && (req.body.teammate_email.length > 0) && ((req.body.relation == "0") || (req.body.relation == "1"))) {
			var query_TeamMate = new Parse.Query(Parse.User);
			query_TeamMate.equalTo("username", req.body.teammate_email);
			query_TeamMate.find ({
				success: function (queryResult_TeamMates) {
					if (queryResult_TeamMates.length == 1) {
						var query_SameTeamName = new Parse.Query("Team");
						query_SameTeamName.equalTo("name", req.body.teamname);
						query_SameTeamName.find ({
							success: function (queryResult_SameTeamName) {
								switch (queryResult_SameTeamName.length) {
									case 0:
										var isTeamSaved = false;
										var isQuestionListGot = false;
										var Team = Parse.Object.extend("Team");
										var newTeam = new Team();
										var questionList = null;
										var query_QuestionList = new Parse.Query("Question");
										query_QuestionList.find({
											success: function (queryResult_QuestionList) {
												questionList = queryResult_QuestionList;
												isQuestionListGot = true;
												setNewTeamQuestions(res, newTeam, questionList, isTeamSaved, isQuestionListGot);
											},
											error: function(error) {
												questionList = null;
												console.error("Error when getting question list: " + error);
											}
										});
										newTeam.set("mark", 0);
										newTeam.set("name", req.body.teamname);
										if (req.body.relation == "0") {
											newTeam.set("younger", currentUser);
											newTeam.set("elder", queryResult_TeamMates[0]);
										} else {
											newTeam.set("younger", queryResult_TeamMates[0]);
											newTeam.set("elder", currentUser);
										}
										newTeam.save(null, {
											success: function(newTeam) {
												isTeamSaved = true;
												setNewTeamQuestions(res, newTeam, questionList, isTeamSaved, isQuestionListGot);
											},
											error: function(newTeam, error) {
												console.error("Error when saving new team object: " + error);
											}
										});
										break;
									case 1:
										res.redirect('/create_team?msg=1');
										break;
									default:
										console.error("Error when getting teams with same name, team count = " + queryResult_SameTeamName.length);
										break;
								}
							},
							error: function (teamNameError) {
								console.error("Error when getting teams with same name: " + teamNameError);
							}
						});
					} else {
						res.redirect('/create_team?msg=2');
					}
				},
				error: function (teamMatesError) {
					console.error("Error when getting team mate with username, error: " + teamMatesError);
				}
			});
		} else {
			res.redirect('/create_team?msg=3');
		}
	} else {
		res.redirect('/login?msg=3');
	}
};

function dashboard_tryRender(res, isBadgeQueryComplete, isQuestionQueryComplete, pageRender_TeamInfo, firstname, badgeInfoArray) {
	if (isBadgeQueryComplete && isQuestionQueryComplete) {
		res.render('dashboard.ejs', {info: pageRender_TeamInfo, firstname: firstname, badges: badgeInfoArray});
	}
}

exports.getDashboardView = function(req, res) {
	var currentUser = Parse.User.current();
	if (currentUser && (0 == currentUser.get("type"))) {
		var isBadgeQueryComplete = false;
		var isQuestionQueryComplete = false;
		var pageRender_TeamInfo = new Array();
		var badgeInfoArray = new Array();
		var firstName = currentUser.get("firstName");
		// query badge info
		var badges = currentUser.relation("badges");
		var badgeCount = currentUser.get("badgeCount");
		if (badges && (badgeCount > 0)) {
			badges.query().find({
				success: function (query_BadgeInfo) {
					for (var bli = 0; bli < query_BadgeInfo.length; bli++) {
						badgeInfoArray.push({name: query_BadgeInfo[bli].get("name"), description: query_BadgeInfo[bli].get("description")});
					}
					isBadgeQueryComplete = true;
					dashboard_tryRender(res, isBadgeQueryComplete, isQuestionQueryComplete, pageRender_TeamInfo, firstName, badgeInfoArray);
				},
				error: function (badgeInfoError) {
					console.error("Error when getting badge info: " + badgeInfoError);
				}
			});
		} else {
			isBadgeQueryComplete = true;
			badgeInfoArray = null;
			//(do not need to render) dashboard_tryRender(res, isBadgeQueryComplete, isQuestionQueryComplete, pageRender_TeamInfo, firstName, badgeInfoArray); 
		}
		// query team and question info
		var query_ElderIsMe = new Parse.Query("Team");
		query_ElderIsMe.equalTo("elder", currentUser);
		var query_YoungerIsMe = new Parse.Query("Team");
		query_YoungerIsMe.equalTo("younger", currentUser);
		var query_MyTeams = Parse.Query.or(query_ElderIsMe, query_YoungerIsMe);
		query_MyTeams.find ({
			success: function (queryResult_MyTeams) {
				var isElderInTeam = new Array();
				for (var i = 0; i < queryResult_MyTeams.length; i++) {
					isElderInTeam[i] = (currentUser.id == queryResult_MyTeams[i].get("elder").id);
					pageRender_TeamInfo[i] = {team_id: queryResult_MyTeams[i].id, team_name: queryResult_MyTeams[i].get("name"), total_mark: queryResult_MyTeams[i].get("mark")};
					pageRender_TeamInfo[i].questions =  new Array();
				}
				var query_QAStates = new Parse.Query("QAState");
				query_QAStates.containedIn("team", queryResult_MyTeams);
				query_QAStates.ascending("createdAt");
				query_QAStates.find ({
					success: function (queryResult_QAState) {
						var unansweredQuestions = new Array();
						for (var i = 0; i < queryResult_QAState.length; i++) {
							for (var j = 0; j < pageRender_TeamInfo.length; j++) {
								if (pageRender_TeamInfo[j].team_id == queryResult_QAState[i].get("team").id) {
									var answered = false;
									if (isElderInTeam[j]) {
										answered = (Constant.QAState.NotAnswered != queryResult_QAState[i].get("elder_state"));
									} else {
										answered = (Constant.QAState.NotAnswered != queryResult_QAState[i].get("younger_state"));
									}
									if (!answered) {
										var question_id = queryResult_QAState[i].get("question").id;
										pageRender_TeamInfo[j].questions.push({question_id: question_id});
										unansweredQuestions.push(question_id);
									}
									break;
								}
							}
						}
						var query_QuestionInfo = new Parse.Query("Question");
						query_QuestionInfo.containedIn("objectId", unansweredQuestions);
						query_QuestionInfo.find ({
							success: function (queryResult_QuestionInfo) {
								for (var i = 0; i < queryResult_QuestionInfo.length; i++) {
									for (var j = 0; j < pageRender_TeamInfo.length; j++) {
										for (var k = 0; k < pageRender_TeamInfo[j].questions.length; k++) {
											if (queryResult_QuestionInfo[i].id == pageRender_TeamInfo[j].questions[k].question_id) {
												pageRender_TeamInfo[j].questions[k].question_name = isElderInTeam[j] ? queryResult_QuestionInfo[i].get("title_elder") : queryResult_QuestionInfo[i].get("title_younger");
												pageRender_TeamInfo[j].questions[k].question_mark = queryResult_QuestionInfo[i].get("mark");
												pageRender_TeamInfo[j].questions[k].question_img = "/images/no-photo.png";
												var imgFile = queryResult_QuestionInfo[i].get("image");
												if (imgFile) {
													var url = imgFile.url();
													if (url) {
														pageRender_TeamInfo[j].questions[k].question_img = url;
													}
												}
												pageRender_TeamInfo[j].questions[k].tags = queryResult_QuestionInfo[i].get("tags");
											}
										}
									}
								}
								isQuestionQueryComplete = true;
								dashboard_tryRender(res, isBadgeQueryComplete, isQuestionQueryComplete, pageRender_TeamInfo, firstName, badgeInfoArray);
							},
							error: function (questionInfoError) {
								console.error("Error when getting question info: " + questionInfoError);
							}
						});
					},
					error: function (qastateError) {
						console.error("Error when getting QAState info: " + qastateError);
					}
				});
			},
			error: function (teamInfoError) {
				console.error("Error when getting team info: " + teamInfoError);
			}
		});
	} else {
		res.redirect('/login?msg=3');
	}
};

function renderQuestion(questionObj, isElder, teamId, useragent, res) {
	var questionType;
	var questionJson;
	var title;
	var description;
	if (isElder) {
		title = questionObj.get("title_elder");
		description = questionObj.get("description_elder");
		questionType = questionObj.get("question_type_elder");
		questionJson = questionObj.get("question_info_elder");
	} else {
		title = questionObj.get("title_younger");
		description = questionObj.get("description_younger");
		questionType = questionObj.get("question_type_younger");
		questionJson = questionObj.get("question_info_younger");
	}
	switch (questionType) {
		case Constant.QustionType.Choice:
			var theQuestion = JSON.parse(questionJson);
			res.render('question-choice.ejs', {title: title, description: description, team_id: teamId, question_id: questionObj.id, choice: theQuestion.choices});
			break;
		case Constant.QustionType.FillInBlank:
			res.render('question-fill-blank.ejs', {title: title, description: description, team_id: teamId, question_id: questionObj.id});
			break;
		case Constant.QustionType.File:
			res.render('question-file-upload.ejs', {title: title, description: description, team_id: teamId, question_id: questionObj.id});
			break;
		case Constant.QustionType.QR:
			// parse user-agent
			var parser = new UAParser();
			var result = parser.setUA(useragent).getResult();
			console.error("ua result: " + JSON.stringify(result));
			var bname = result.browser.name;
			var view;
			if ((bname == "Chrome" || bname == "Chromium" || bname == "Firefox" || bname.substr(0, 5) == "Opera") && (result.os.name != "iOS")) {
				view = 'question-qr-camera.ejs';
			} else {
				view = 'question-qr-image.ejs';
			}
			res.render(view, {title: title, description: description, team_id: teamId, question_id: questionObj.id});
			break;
		default:
			console.error("Error: Unrecognized question type: " + questionType);
			break;
	}
}

exports.getQuestionView = function(req, res) {
	var currentUser = Parse.User.current();
	if (currentUser && (0 == currentUser.get("type"))) {
		var get_MyTeam = new Parse.Query("Team");
		get_MyTeam.get (req.params.team_id, {
			success: function (myTeam) {
				var user_id = currentUser.id;
				var isElder = (user_id == myTeam.get("elder").id);
				var isYounger = (user_id == myTeam.get("younger").id);
				if (isElder || isYounger) {
					var get_ThisQuestion = new Parse.Query("Question");
					get_ThisQuestion.get (req.params.question_id, {
						success: function (thisQuestion) {
							renderQuestion(thisQuestion, isElder, myTeam.id, req.headers['user-agent'], res);
						},
						error: function (questionInfoError) {
							console.error("Error when getting question info: " + questionInfoError);
						}
					});
				} else {
					console.error("Error when getting team info, not in the team");
				}
			},
			error: function (team_error) {
				console.error("Error when getting team info: " + team_error);
			}
		});
	} else {
		res.redirect('/login?msg=3');
	}
};

function handleAnswers(questionObj, isElder, myTeamObj, req, res) {
	var questionType;
	var markingType;
	var theQuestion;
	var title;
	var description;
	if (isElder) {
		title = questionObj.get("title_elder");
		description = questionObj.get("description_elder");
		questionType = questionObj.get("question_type_elder");
		markingType = questionObj.get("marking_type_elder");
		theQuestion = JSON.parse(questionObj.get("question_info_elder"));
	} else {
		title = questionObj.get("title_younger");
		description = questionObj.get("description_younger");
		questionType = questionObj.get("question_type_younger");
		markingType = questionObj.get("marking_type_younger");
		theQuestion = JSON.parse(questionObj.get("question_info_younger"));
	}

	var userTargetState;
	var userAnswer;

	switch (questionType) {
		case Constant.QustionType.Choice:
			userAnswer = req.body.choice;
			break;
		case Constant.QustionType.FillInBlank:
			userAnswer = req.body.answer;
			break;
		case Constant.QustionType.File:
			userAnswer = req.body.answer;
			break;
		case Constant.QustionType.QR:
			userAnswer = req.body.answer;
			break;
		default:
			console.error("Error: Unrecognized question type: " + questionType);
			return;
	}

	switch (markingType) {
		case Constant.MarkingType.Auto:
			switch (questionType) {
				case Constant.QustionType.Choice:
					var user_choice = parseInt(userAnswer);
					userTargetState = (theQuestion.answer == user_choice) ? Constant.QAState.Correct : Constant.QAState.Wrong;
					break;
				case Constant.QustionType.FillInBlank:
					userTargetState = (theQuestion.answer == userAnswer) ? Constant.QAState.Correct : Constant.QAState.Wrong;
					break;
				case Constant.QustionType.File:
					userTargetState = Constant.QAState.WaitingMarking
					break;
				case Constant.QustionType.QR:
					userTargetState = (theQuestion.answer == userAnswer) ? Constant.QAState.Correct : Constant.QAState.Wrong;
					break;
				default:
					console.error("Error: Unrecognized question type: " + questionType);
					return;
			}
			break;
		case Constant.MarkingType.Manual:
			userTargetState = Constant.QAState.WaitingMarking;
			break;
		case Constant.MarkingType.Invalid:
		default:
			console.error("Error: Unrecognized question marking type: " + markingType);
			return;
	}

	var query_QAStates = new Parse.Query("QAState");
	query_QAStates.equalTo("team", myTeamObj);
	query_QAStates.equalTo("question", questionObj);
	query_QAStates.find ({
		success: function (queryResult_QAState) {
			if (1 == queryResult_QAState.length) {
				var theState = queryResult_QAState[0];
				if (isElder) {
					theState.set("elder_state", userTargetState);
					theState.set("elder_answer", userAnswer);
				} else {
					theState.set("younger_state", userTargetState);
					theState.set("younger_answer", userAnswer);
				}
				theState.save({
					success: function (obj) {
						res.redirect('/dashboard');
					},
					error: function (obj, uperror) {
						console.error("Error when updating QAState info: " + uperror);
					}
				});
			} else {
				console.error("Error when getting QAState info, not 1 result");
			}
		},
		error: function (QAStateError) {
			console.error("Error when getting QAState info: " + QAStateError);
		}
	});
}

exports.answerQuestion = function(req, res) {
	var currentUser = Parse.User.current();
	if (currentUser && (0 == currentUser.get("type"))) {
		var get_MyTeam = new Parse.Query("Team");
		get_MyTeam.get (req.params.team_id, {
			success: function (myTeam) {
				var user_id = currentUser.id;
				if ((user_id == myTeam.get("elder").id) || (user_id == myTeam.get("younger").id)) {
					var isElder = (user_id == myTeam.get("elder").id);
					var get_ThisQuestion = new Parse.Query("Question");
					get_ThisQuestion.get (req.params.question_id, {
						success: function (thisQuestion) {
							handleAnswers(thisQuestion, isElder, myTeam, req, res)
						},
						error: function (questionInfoError) {
							console.error("Error when getting question info: " + questionInfoError);
						}
					});
				} else {
					console.error("Error when getting team info, not in the team");
				}
			},
			error: function (team_error) {
				console.error("Error when getting team info: " + team_error);
			}
		});
	} else {
		res.redirect('/login?msg=3');
	}
};