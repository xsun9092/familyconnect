var Constant = require('cloud/util/constant');

exports.getLoginView = function(req, res) {
	res.render('admin-login.ejs', {msg: req.query.msg});
};

exports.login = function(req, res) {
	Parse.User.logIn(req.body.username, req.body.password, {
		success: function(user) {
			if (Constant.UserType.Admin == user.get("type")) {
				res.redirect('/admin/dashboard');
			} else {
				res.redirect('/admin-login?msg=1');
			}
		},
		error: function(user, error) {
			res.redirect('/admin-login?msg=1');
		}
	});
};

exports.checkAdmin = function(req, res, next) {
	var currentAdmin = Parse.User.current();
	if (currentAdmin && (Constant.UserType.Admin == currentAdmin.get("type"))) {
		next();
	} else {
		res.redirect('/admin-login?msg=3');
	}
};