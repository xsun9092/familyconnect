
// These two lines are required to initialize Express in Cloud Code.
var express = require('express');
var parseExpressHttpsRedirect = require('parse-express-https-redirect');
var parseExpressCookieSession = require('parse-express-cookie-session');
var app = express();

// Routes
const userAccount = require('cloud/controllers/user-account');
const userFunction = require('cloud/controllers/user-function');
const adminAccount = require('cloud/controllers/admin-account');
const adminFunction = require('cloud/controllers/admin-function');

// Global app configuration section
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(parseExpressHttpsRedirect());  // Require user to be on HTTPS.
app.use(express.bodyParser());
app.use(express.cookieParser('FaMiLyCoNeCtSeCrIt'));
app.use(parseExpressCookieSession({ cookie: { maxAge: 3600000 }, fetchUser: true }));


// normal users
app.get('/register', userAccount.getRegisterView);
app.post('/register', userAccount.register);
app.get('/login', userAccount.getLoginView);
app.post('/login', userAccount.login);
app.get('/logout', userAccount.logout);

app.get('/create_team', userFunction.getCreateTeamView);
app.post('/create_team', userFunction.createTeam);

app.get('/dashboard', userFunction.getDashboardView);

app.get('/question/:team_id/:question_id', userFunction.getQuestionView);
app.post('/question/:team_id/:question_id', userFunction.answerQuestion);


// admin
app.get('/admin-login', adminAccount.getLoginView);
app.post('/admin-login', adminAccount.login);
app.all('/admin/*', adminAccount.checkAdmin);


app.get('/admin/dashboard', adminFunction.getDashboardView);
app.get('/admin/teams', adminFunction.getTeamListView);
app.get('/admin/users', adminFunction.getUserListView);
app.get('/admin/view-user/:user_id', adminFunction.getSingleUserView);
app.get('/admin/view-team/:team_id', adminFunction.getSingleTeamView);
app.get('/admin/new-question', adminFunction.getNewQuestionView);
app.post('/admin/new-question', adminFunction.createNewQuestion);
app.get('/admin/need-marking', adminFunction.getNeedMarking);
app.get('/admin/mark/:qaState_id', adminFunction.getMarkingQuestionView);
app.post('/admin/mark/:qaState_id', adminFunction.markQuestion);

// Attach the Express app to Cloud Code.
app.listen();
